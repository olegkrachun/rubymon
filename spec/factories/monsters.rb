FactoryGirl.define do
  factory :monster do
    sequence(:name) { |n| "Illidan #{n}" }
    power     '50'
    sub_class 'fire'
    user
  end
end
