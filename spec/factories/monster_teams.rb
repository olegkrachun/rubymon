FactoryGirl.define do
  factory :monster_team do
    sequence(:name) { |n| "Orcs #{n}" }
    user
    after(:build) do |team|
      team.monsters << build_list(:monster, 3, user_id: team.user_id)
    end
  end

  factory :invalid_monster_team, class: MonsterTeam do
    name 'Insufficient count of team members'
  end
end
