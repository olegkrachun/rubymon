require 'rails_helper'

RSpec.describe MonsterTeamsController, type: :controller do
  include Devise::TestHelpers

  let(:user) { FactoryGirl.create(:user) }
  before { sign_in user }

  describe "GET #index" do
    let!(:team) { FactoryGirl.create(:monster_team, user: user) }

    it "assigns all user teams as @teams" do
      get :index
      expect(assigns(:teams)).to eq([team])
    end

    it 'should not include non user teams' do
      other_team = FactoryGirl.create(:monster_team)

      get :index
      expect(assigns[:teams]).not_to include(other_team)
    end
  end

  describe "GET #show" do
    let(:team) { FactoryGirl.create(:monster_team, user: user) }

    it "assigns the requested monster as @monster" do
      get :show, id: team.to_param
      expect(assigns(:team)).to eq(team)
    end
  end

  describe "GET #new" do
    it "assigns a new team as @team" do
      get :new
      expect(assigns(:team)).to be_a_new(MonsterTeam)
    end
  end

  describe "POST #create" do
    let(:params) { FactoryGirl.attributes_for(:monster_team) }

    context "with valid params" do
      before do
        3.times { FactoryGirl.create(:monster) }
        params.merge!(monster_ids: Monster.pluck(:id))
      end

      it "creates a new Monster Team" do
        expect { post :create, monster_team: params }
                                          .to change(MonsterTeam, :count).by(1)
      end

      it "assigns a newly created team as @team" do
        post :create, monster_team: params

        expect(assigns(:team)).to be_a(MonsterTeam)
        expect(assigns(:team)).to be_persisted
      end

      it "redirects to the created team" do
        post :create, monster_team: params
        expect(response).to redirect_to(MonsterTeam.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved team as @team" do
        post :create, monster_team: params
        expect(assigns(:team)).to be_a_new(MonsterTeam)
      end

      it "re-renders the 'new' template" do
        post :create, monster_team: params
        expect(response).to render_template("new")
      end
    end
  end

  describe "GET #edit" do
    let!(:team) { FactoryGirl.create(:monster_team, user: user) }

    it "assigns the requested team as @team" do
      get :edit, id: team.to_param
      expect(assigns(:team)).to eq(team)
    end
  end

  describe "PUT #update" do
    let!(:team) { FactoryGirl.create(:monster_team, user: user) }

    context "with valid params" do
      let(:new_attributes) { Hash(name: 'Avengers') }

      it "updates the requested team" do
        put :update, { id: team.to_param, monster_team: new_attributes }
        expect(team.reload.name).to eq(new_attributes[:name])
      end

      it "assigns the requested team as @team" do
        put :update, { id: team.to_param, monster_team: new_attributes }
        expect(assigns(:team)).to eq(team)
      end

      it "redirects to the monster team" do
        put :update, { id: team.to_param, monster_team: new_attributes }
        expect(response).to redirect_to(team)
      end
    end

    context "with invalid params" do
      let(:invalid_params) { Hash(name: '') }
      before { allow_any_instance_of(Monster).to receive(:valid?) { false } }

      it "assigns the team as @team" do
        put :update, { id: team.to_param, monster_team: invalid_params }
        expect(assigns(:team)).to eq(team)
      end

      it "re-renders the 'edit' template" do
        put :update, { id: team.to_param, monster_team: invalid_params }
        expect(response).to render_template("edit")
      end
    end
  end

  describe "GET #destroy" do
    let!(:team) { FactoryGirl.create(:monster_team, user: user) }

    it "destroys the requested monster team" do
      expect {
        delete :destroy, id: team.to_param
      }.to change(MonsterTeam, :count).by(-1)
    end

    it "redirects to the monsters list" do
      delete :destroy, id: team.to_param
      expect(response).to redirect_to(monster_teams_url)
    end
  end
end
