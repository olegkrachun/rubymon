require 'rails_helper'

RSpec.describe Api::V1::MonsterTeamsController, type: :controller do

  let!(:api_token) { FactoryGirl.create(:api_token).access_token }
  let(:header)     { Hash('HTTP_AUTHORIZATION' => "Token token=#{api_token}") }

  before           { @request.env.merge!(header) }

  describe 'GET #index' do
    let!(:team) { FactoryGirl.create(:monster_team) }
    let!(:user)  { team.user }

    it 'should return monster team' do
      get :index, user_email: user.email

      expect(JSON.parse(response.body)).to eq({
          'monster_teams' => [{
              'id'        => team.id,
              'name'      => team.name,
              'monsters'  => [{
                      'id'        => team.monsters.first.id,
                      'name'      => team.monsters.first.name,
                      'power'     => team.monsters.first.power,
                      'sub_class' => team.monsters.first.sub_class
                  }, {
                      'id'        => team.monsters.second.id,
                      'name'      => team.monsters.second.name,
                      'power'     => team.monsters.second.power,
                      'sub_class' => team.monsters.second.sub_class
                  }, {
                      'id'        => team.monsters.last.id,
                      'name'      => team.monsters.last.name,
                      'power'     => team.monsters.last.power,
                      'sub_class' => team.monsters.last.sub_class
              }]
          }]
      })
    end

    it 'should return http status 200' do
      get :index, user_email: user.email
      expect(response.status).to eq(200)
    end

    it 'should not return teams that do not belong to the user' do
      user2 = FactoryGirl.create(:user)
      team.update(user: user2)
      get :index, user_email: user.email

      expect(JSON.parse(response.body)).to eq({'monster_teams' => []})
    end
  end

  describe 'GET #show' do
    let(:team)   { FactoryGirl.create(:monster_team) }

    it 'should return monster team info' do
      get :show, id: team.id

      expect(JSON.parse(response.body)).to eq({
          'monster_team' => {
              'id'        => team.id,
              'name'      => team.name,
              'monsters'  => [{
                      'id'        => team.monsters.first.id,
                      'name'      => team.monsters.first.name,
                      'power'     => team.monsters.first.power,
                      'sub_class' => team.monsters.first.sub_class
                  }, {
                      'id'        => team.monsters.second.id,
                      'name'      => team.monsters.second.name,
                      'power'     => team.monsters.second.power,
                      'sub_class' => team.monsters.second.sub_class
                  }, {
                      'id'        => team.monsters.last.id,
                      'name'      => team.monsters.last.name,
                      'power'     => team.monsters.last.power,
                      'sub_class' => team.monsters.last.sub_class
              }]
          }
      })
    end

    it 'should return http status 200' do
      get :show, id: team.id
      expect(response.status).to eq(200)
    end
  end

  describe 'POST #create' do
    let(:user)    { FactoryGirl.create(:user) }

    context 'when valid request' do
      let(:params) { FactoryGirl.attributes_for(:monster_team, user_id: user.id) }
      let(:team)   { MonsterTeam.last }

      before do
        3.times { FactoryGirl.create(:monster, user: user) }
        params.merge!(monster_ids: Monster.pluck(:id))
      end

      it 'should create team' do
        expect { post :create, monster_team: params }
            .to change { MonsterTeam.count }.by(1)
      end

      it 'should have status 201 as created entity' do
        post :create, monster_team: params
        expect(response.status).to eq(201)
      end

      it 'should return newly created team' do
        post :create, monster_team: params
        expect(JSON.parse(response.body)['monster_team']).to be_present
      end
    end

    context 'when invalid request' do
      let(:invalid_params) { FactoryGirl.attributes_for(:monster_team,
                                                        user_id: user.id,
                                                        name: '') }

      it 'should return status 422 of unprocessable entity' do
        post :create, monster_team: invalid_params
        expect(response.status).to eq(422)
      end

      it 'should return errors' do
        post :create, monster_team: invalid_params
        expect(JSON.parse(response.body)).to eq({
            'monster_team' => {
                'errors' => {
                    'name' => ['can\'t be blank'],
                    'size' => ['should be equal to 3 monsters']
                }
            }
        })
      end
    end
  end

  describe 'PUT #update' do
    let(:team) { FactoryGirl.create(:monster_team) }

    context 'when valid request' do
      let(:valid_params) { Hash(name: 'New Monster Team') }

      it 'should update team' do
        expect { patch :update, id: team.id, monster_team: valid_params }
            .to change { team.reload.name }.to(valid_params[:name])
      end

      it 'should return status success' do
        patch :update, id: team.id, monster_team: valid_params
        expect(response.status).to eq(200)
      end

      it 'it should return updated team' do
        patch :update, id: team.id, monster_team: valid_params
        expect(JSON.parse(response.body)['monster_team']).to be_present
      end
    end

    context 'when invalid request' do
      let(:invalid_params) { Hash(name: '') }

      it 'should not update team' do
        expect { patch :update, id: team.id, monster_team: invalid_params }
            .not_to change { team.reload.name }
      end

      it 'should return status 422 of unprocessable entity' do
        patch :update, id: team.id, monster_team: invalid_params
        expect(response.status).to eq(422)
      end

      it 'should return errors' do
        patch :update, id: team.id, monster_team: invalid_params
        expect(JSON.parse(response.body)).to eq({
            'monster_team' => {
                'errors' => {
                    'name' => ['can\'t be blank']
                }
            }
        })
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:team) { FactoryGirl.create(:monster_team) }

    context 'when valid request' do
      it 'should delete team' do
        expect { delete :destroy, id: team.id }
            .to change { MonsterTeam.count }.by(-1)
      end

      it 'should return http status 200' do
        delete :destroy, id: team.id
        expect(response.status).to eq(200)
      end
    end

    context 'when invalid request' do
      before { allow_any_instance_of(MonsterTeam).to receive(:destroy) { false } }

      it 'should mot destroy team' do
        expect { delete :destroy, id: team.id }
            .not_to change { MonsterTeam.count }
      end

      it 'should return http status 422 of unprocessable entity' do
        delete :destroy, id: team.id
        expect(response.status).to eq(422)
      end

      it 'should return an error' do
        delete :destroy, id: team.id
        expect(JSON.parse(response.body)).to eq({
            'monster_team' => {
                'errors' => 'Monster Team can\'t be deleted'
            }
        })
      end
    end
  end
end
