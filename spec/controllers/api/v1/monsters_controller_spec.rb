require 'rails_helper'

RSpec.describe Api::V1::MonstersController, type: :controller do

  let!(:api_token) { FactoryGirl.create(:api_token).access_token }
  let(:header)     { Hash('HTTP_AUTHORIZATION' => "Token token=#{api_token}") }

  before           { @request.env.merge!(header) }

  describe 'GET #index' do
    let(:user)     { FactoryGirl.create(:user) }
    let!(:monster) { FactoryGirl.create(:monster, user: user) }

    it 'should return monsters' do
      get :index, user_email: user.email

      expect(JSON.parse(response.body)).to eq({
        'monsters' => [{
            'id'        => monster.id,
            'name'      =>monster.name,
            'power'     =>monster.power,
            'sub_class' =>monster.sub_class
       }]
      })
    end

    it 'should return http status 200' do
      get :index, user_email: user.email
      expect(response.status).to eq(200)
    end

    it 'should not return monsters that do not belong to the user' do
      user2 = FactoryGirl.create(:user)
      monster.update(user: user2)
      get :index, user_email: user.email

      expect(JSON.parse(response.body)).to eq({'monsters' => []})
    end
  end

  describe 'GET #show' do
    let(:monster)   { FactoryGirl.create(:monster) }

    it 'should return monster info' do
      get :show, id: monster.id

      expect(JSON.parse(response.body)).to eq({
        'monster' => {
            'id'        => monster.id,
            'name'      => monster.name,
            'power'     => monster.power,
            'sub_class' => monster.sub_class
        }
      })
    end

    it 'should return http status 200' do
      get :show, id: monster.id
      expect(response.status).to eq(200)
    end

    it 'should return empty object once record is not found' do
      get :show, id: 'unknown'

      expect(JSON.parse(response.body)).to eq('monster' => nil)
    end
  end

  describe 'POST #create' do
    let(:user)    { FactoryGirl.create(:user) }

    context 'when valid request' do
      let(:params)  { FactoryGirl.attributes_for(:monster) }
      let(:monster) { Monster.last }

      it 'should create monster' do
        expect { post :create, monster: params.merge(user_id: user.id) }.
            to change { Monster.count }.by(1)
      end

      it 'should have status 201 as created entity' do
        post :create, monster: params.merge(user_id: user.id)
        expect(response.status).to eq(201)
      end

      it 'should return newly created monster' do
        post :create, monster: params.merge(user_id: user.id)
        expect(JSON.parse(response.body)).to eq({
            'monster' => {
                'id'        => monster.id,
                'name'      => monster.name,
                'power'     => monster.power,
                'sub_class' => monster.sub_class
            }
        })
      end
    end

    context 'when invalid request' do
      let(:invalid_params) { FactoryGirl.attributes_for(:monster,
                                                        user_id: user.id,
                                                        name: '') }

      it 'should return status 422 of unprocessable entity' do
        post :create, monster: invalid_params
        expect(response.status).to eq(422)
      end

      it 'should return errors' do
        post :create, monster: invalid_params
        expect(JSON.parse(response.body)).to eq({
            'monster' => {
                'errors' => {
                    'name' => ['can\'t be blank']
                }
            }
        })
      end
    end
  end

  describe 'PUT #update' do
    let(:user)      { FactoryGirl.create(:user) }
    let(:monster)   { FactoryGirl.create(:monster, user: user) }

    context 'when valid request' do
      let(:valid_params) { Hash(name: 'New Monster Name') }

      it 'should update monster' do
        expect { patch :update, id: monster.id, monster: valid_params }
            .to change { monster.reload.name }.to valid_params[:name]
      end

      it 'should return status success' do
        patch :update, id: monster.id, monster: valid_params
        expect(response.status).to eq(200)
      end

      it 'it should return updated monster' do
        patch :update, id: monster.id, monster: valid_params
        monster.reload
        expect(JSON.parse(response.body)).to eq({
            'monster' => {
                'id'        => monster.id,
                'name'      => monster.name,
                'power'     => monster.power,
                'sub_class' => monster.sub_class
            }
        })
      end
    end

    context 'when invalid request' do
      let(:invalid_params) { Hash(name: '') }

      it 'should not update monster' do
        expect { patch :update, id: monster.id, monster: invalid_params }
          .not_to change { monster.reload.name }
      end

      it 'should return status 422 of unprocessable entity' do
        patch :update, id: monster.id, monster: invalid_params
        expect(response.status).to eq(422)
      end

      it 'should return errors' do
        patch :update, id: monster.id, monster: invalid_params
        expect(JSON.parse(response.body)).to eq({
            'monster' => {
                'errors' => {
                    'name' => ['can\'t be blank']
                }
            }
        })
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:monster)     { FactoryGirl.create(:monster) }

    context 'when valid request' do
      it 'should delete monster' do
        expect { delete :destroy, id: monster.id }
            .to change { Monster.count }.by(-1)
      end

      it 'should return http status 200' do
        delete :destroy, id: monster.id
        expect(response.status).to eq(200)
      end
    end

    context 'when invalid request' do
      before { allow_any_instance_of(Monster).to receive(:destroy) { false } }

      it 'should mot destroy monster' do
        expect { delete :destroy, id: monster.id }
            .not_to change { Monster.count }
      end

      it 'should return http status 422 of unprocessable entity' do
        delete :destroy, id: monster.id
        expect(response.status).to eq(422)
      end

      it 'should return an error' do
        delete :destroy, id: monster.id
        expect(JSON.parse(response.body)).to eq({
            'monster' => {
                'errors' => 'Monster can\'t be deleted'
            }
        })
      end
    end
  end
end
