require 'rails_helper'

RSpec.describe MonstersController, type: :controller do
  include Devise::TestHelpers

  let(:user) { FactoryGirl.create(:user) }
  before { sign_in user }

  describe "GET #index" do
    let!(:monster) { FactoryGirl.create(:monster, user: user) }

    it "assigns all monsters as @monsters" do
      get :index
      expect(assigns(:monsters)).to eq([monster])
    end

    it 'should not select monsters that do not belong to user' do
      other_monster = FactoryGirl.create(:monster)

      get :index
      expect(assigns[:monsters]).not_to include(other_monster)
    end
  end

  describe "GET #show" do
    let!(:monster) { FactoryGirl.create(:monster, user: user) }

    it "assigns the requested monster as @monster" do
      get :show, id: monster.to_param
      expect(assigns(:monster)).to eq(monster)
    end
  end

  describe "GET #new" do
    it "assigns a new monster as @monster" do
      get :new
      expect(assigns(:monster)).to be_a_new(Monster)
    end
  end

  describe "GET #edit" do
    let!(:monster) { FactoryGirl.create(:monster, user: user) }

    it "assigns the requested monster as @monster" do
      get :edit, id: monster.to_param
      expect(assigns(:monster)).to eq(monster)
    end
  end

  describe "POST #create" do
    let(:params) { FactoryGirl.attributes_for(:monster, user: user) }

    context "with valid params" do
      it "creates a new Monster" do
        expect {
          post :create, monster: params
        }.to change(Monster, :count).by(1)
      end

      it "assigns a newly created monster as @monster" do
        post :create, monster: params

        expect(assigns(:monster)).to be_a(Monster)
        expect(assigns(:monster)).to be_persisted
      end

      it "redirects to the created monster" do
        post :create, monster: params
        expect(response).to redirect_to(Monster.last)
      end
    end

    context "with invalid params" do
      before { allow_any_instance_of(Monster).to receive(:valid?) { false } }

      it "assigns a newly created but unsaved monster as @monster" do
        post :create, monster: params
        expect(assigns(:monster)).to be_a_new(Monster)
      end

      it "re-renders the 'new' template" do
        post :create, monster: params
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    let!(:monster) { FactoryGirl.create(:monster, user: user) }

    context "with valid params" do
      let(:new_attributes) { Hash(name: 'Rexxar') }

      it "updates the requested monster" do
        put :update, { id: monster.to_param, monster: new_attributes }
        monster.reload
        expect(monster.name).to eq(new_attributes[:name])
      end

      it "assigns the requested monster as @monster" do
        put :update, { id: monster.to_param, monster: new_attributes }
        expect(assigns(:monster)).to eq(monster)
      end

      it "redirects to the monster" do
        put :update, { id: monster.to_param, monster: new_attributes }
        expect(response).to redirect_to(monster)
      end
    end

    context "with invalid params" do
      let(:invalid_params) { Hash(name: '') }
      before { allow_any_instance_of(Monster).to receive(:valid?) { false } }

      it "assigns the monster as @monster" do
        put :update, { id: monster.to_param, monster: invalid_params }
        expect(assigns(:monster)).to eq(monster)
      end

      it "re-renders the 'edit' template" do
        put :update, { id: monster.to_param, monster: invalid_params }
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:monster) { FactoryGirl.create(:monster, user: user) }

    it "destroys the requested monster" do
      expect {
        delete :destroy, id: monster.to_param
      }.to change(Monster, :count).by(-1)
    end

    it "redirects to the monsters list" do
      delete :destroy, id: monster.to_param
      expect(response).to redirect_to(monsters_url)
    end
  end
end
