require 'rails_helper'

RSpec.describe Monster, type: :model do

  describe '#associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:monster_team)}
  end

  describe '#validators' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:power) }
    it { is_expected.to validate_presence_of(:sub_class) }
    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.to validate_inclusion_of(:sub_class)
                            .in_array(described_class::SUB_CLASSES) }

    describe '#validate_monsters_size' do
      let(:user) { FactoryGirl.create(:user) }

      it 'should not allow to create more than 20 monsters for one user' do
        20.times { FactoryGirl.create(:monster, user: user) }
        extra_monster = FactoryGirl.build(:monster, user: user)

        expect(extra_monster).not_to be_valid
        expect(extra_monster.errors.full_messages)
            .to include('Monsters\' number is exceeded. User should not have more than 20 monsters')
      end

      it 'should allow to create less than 20 monsters for one user' do
        monster = FactoryGirl.build(:monster, user: user)
        expect(monster).to be_valid
      end
    end
  end

  describe '#define_weakness' do
    let(:monster) { FactoryGirl.build(:monster, sub_class: 'fire') }

    it 'should define the correct weakness' do
      expect { monster.save }.to change { monster.weakness }.to('water')
    end

    it 'should define the correct weakness for last element in the list' do
      monster.sub_class = 'wind'
      expect { monster.save }.to change { monster.weakness }.to('fire')
    end
  end
end
