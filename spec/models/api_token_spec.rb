require 'rails_helper'

RSpec.describe ApiToken, type: :model do

  describe '#validations' do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe '#set_token' do
    it 'should create token when creating the record' do
      record = FactoryGirl.build(:api_token)

      expect { record.save }.to change { record.access_token }
      expect(record.reload.access_token).not_to be_nil
    end
  end
end
