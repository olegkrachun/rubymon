require 'rails_helper'

RSpec.describe User, type: :model do

  describe '#associations' do
    it { is_expected.to have_many(:monsters) }
    it { is_expected.to have_many(:monster_teams) }
  end
end
