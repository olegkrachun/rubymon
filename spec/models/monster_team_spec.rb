require 'rails_helper'

RSpec.describe MonsterTeam, type: :model do
  describe '#associations' do
    it { is_expected.to have_many(:monsters) }
    it { is_expected.to belong_to(:user) }
  end

  describe '#validators' do
    it { is_expected.to validate_presence_of(:name) }

    context 'when validating the team size' do
      let!(:team) { FactoryGirl.build(:invalid_monster_team) }

      it 'should allow three monsters in the team' do
        3.times { team.monsters << FactoryGirl.create(:monster) }
        expect(team).to be_valid
      end

      it 'should not allow less then three monsters in the team' do
        2.times { team.monsters << FactoryGirl.create(:monster) }

        expect(team).not_to be_valid
        expect(team.errors.full_messages).to include('Size should be equal to 3 monsters')
      end

      it 'should not allow more than three monsters in the team' do
        4.times { team.monsters << FactoryGirl.create(:monster) }

        expect(team).not_to be_valid
        expect(team.errors.full_messages).to include('Size should be equal to 3 monsters')
      end
    end

    describe 'user can have up to 3 teams' do
      let(:user) { FactoryGirl.create(:user) }

      before { 3.times { FactoryGirl.create(:monster_team, user: user) } }

      it 'should not allow user to have more than 3 teams' do
        extra_team = FactoryGirl.build(:monster_team, user_id: user.id)

        expect(extra_team).not_to be_valid
        expect(extra_team.errors.full_messages).to include('User is limited to 3 teams')
      end
    end
  end
end
