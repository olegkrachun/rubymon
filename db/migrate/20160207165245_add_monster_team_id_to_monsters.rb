class AddMonsterTeamIdToMonsters < ActiveRecord::Migration
  def change
    add_column  :monsters, :monster_team_id, :integer
    add_index   :monsters, :monster_team_id
  end
end
