class CreateMonsterTeams < ActiveRecord::Migration
  def change
    create_table :monster_teams do |t|
      t.string      :name
      t.integer     :user_id
      t.timestamps  null: false
    end

    add_index :monster_teams, :user_id
  end
end
