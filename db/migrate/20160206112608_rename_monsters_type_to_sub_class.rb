class RenameMonstersTypeToSubClass < ActiveRecord::Migration
  def change
    rename_column :monsters, :type, :sub_class
  end
end
