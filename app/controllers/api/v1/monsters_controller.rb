module Api
  module V1
    class MonstersController < ApiController
      def index
        user = User.find_by(email: params[:user_email])
        render json: user.try(:monsters), each_serializer: MonsterSerializer
      end

      def show
        monster = Monster.find_by(id: params[:id])
        render json: monster, serializer: MonsterSerializer
      end

      def create
        monster = Monster.new(monster_params)
        if monster.save
          render status: 201, json: monster, serializer: MonsterSerializer
        else
          render status: 422, json: { monster: { errors: monster.errors } }
        end
      end

      def update
        monster = Monster.find_by(id: params[:id])
        if monster.update(monster_params)
          render status: 200, json: monster, serializer: MonsterSerializer
        else
          render status: 422, json: { monster: { errors: monster.errors } }
        end
      end

      def destroy
        monster = Monster.find_by(id: params[:id])
        if monster.destroy
          render status: 200, json: {}
        else
          render status: 422, json: { monster: {
                                          errors: 'Monster can\'t be deleted' }}
        end
      end

      private

      def monster_params
        params.require(:monster).permit(:name, :power, :sub_class, :user_id)
      end
    end
  end
end
