module Api
  module V1
    class MonsterTeamsController < ApiController

      def index
        user = User.find_by(email: params[:user_email])
        render json: user.monster_teams.includes(:monsters),
               each_serializer: MonsterTeamSerializer
      end

      def show
        team = MonsterTeam.includes(:monsters).find_by(id: params[:id])
        render json: team, serializer: MonsterTeamSerializer
      end

      def create
        team = MonsterTeam.new(monster_team_params)

        if team.save
          render status: 201, json: team, serializer: MonsterTeamSerializer
        else
          render status: 422, json: { monster_team: { errors: team.errors } }
        end
      end

      def update
        team = MonsterTeam.find_by(id: params[:id])

        if team.update(monster_team_params)
          render status: 200, json: team, serializer: MonsterTeamSerializer
        else
          render status: 422, json: { monster_team: { errors: team.errors } }
        end
      end

      def destroy
        team = MonsterTeam.find_by(id: params[:id])

        if team.destroy
          render status: 200, json: {}
        else
          render status: 422, json: { monster_team: {
                                errors: 'Monster Team can\'t be deleted' }}
        end
      end

      private

      def monster_team_params
        params.require(:monster_team).permit(:name, :user_id, monster_ids: [])
      end
    end
  end
end
