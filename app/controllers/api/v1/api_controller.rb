module Api
  module V1
    class ApiController < ApplicationController
      skip_filter :authenticate_user!
      before_filter :authenticate_request

      serialization_scope :view_context

      rescue_from Exception, :with => :render_error

      respond_to :json

      private

      def authenticate_request
        authenticate_or_request_with_http_token do |token, options|
          ApiToken.find_by(access_token: token)
        end
      end

      def render_error
        render nothing: true, status: 400
      end
    end
  end
end
