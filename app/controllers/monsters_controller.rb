class MonstersController < ApplicationController
  before_action :set_monster, only: [:show, :edit, :update, :destroy]

  def index
    @q = current_user.monsters.ransack(params[:q])
    @monsters = @q.result
  end

  def show
  end

  def new
    @monster = current_user.monsters.new
  end

  def edit
  end

  def create
    @monster = current_user.monsters.new(monster_params)

    if @monster.save
      redirect_to @monster, notice: 'Monster was successfully created.'
    else
      render :new
    end
  end

  def update
    if @monster.update(monster_params)
      redirect_to @monster, notice: 'Monster was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @monster.destroy
    redirect_to monsters_url, notice: 'Monster was successfully destroyed.'
  end

  private
    def set_monster
      @monster = current_user.monsters.find(params[:id])
    end

    def monster_params
      params.require(:monster).permit(:name, :power, :sub_class)
    end
end
