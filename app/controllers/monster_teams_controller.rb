class MonsterTeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]

  def index
    @teams = current_user.monster_teams
  end

  def show
  end

  def new
    @team = current_user.monster_teams.new
  end

  def create
    @team = current_user.monster_teams.new(monster_team_params)

    if @team.save
      redirect_to @team, notice: 'Monster Team was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @team.update(monster_team_params)
      redirect_to @team, notice: 'Monster Team was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @team.destroy
    redirect_to monster_teams_url, notice: 'Monster Team was successfully destroyed.'
  end

  private

  def set_team
    @team = current_user.monster_teams.find(params[:id])
  end

  def monster_team_params
    params.require(:monster_team).permit(:name, monster_ids: [])
  end
end
