class MonsterSerializer < ActiveModel::Serializer
  attributes :id, :name, :power, :sub_class
end
