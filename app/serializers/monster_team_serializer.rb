class MonsterTeamSerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :monsters, serializer: MonsterSerializer
end
