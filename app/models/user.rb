class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, omniauth_providers: [:facebook]

  has_many :monsters
  has_many :monster_teams, -> { includes :monsters }

  def self.from_omniauth(omniauth_params)
    where( provider: omniauth_params.provider,
           uid: omniauth_params.uid
    ).first_or_create do |user|
      user.email =    omniauth_params.info.email
      user.password = Devise.friendly_token[0,20]
    end
  end
end
