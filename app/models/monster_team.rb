class MonsterTeam < ActiveRecord::Base

  belongs_to  :user
  has_many    :monsters

  validates :name, presence: true
  validate :validate_team_size
  validate :user_team_limit

  private

  def validate_team_size
    errors.add(:size, 'should be equal to 3 monsters') if monsters.size != 3
  end

  def user_team_limit
    if user && user.monster_teams.count >= 3
      errors.add(:base, 'User is limited to 3 teams')
    end
  end
end
