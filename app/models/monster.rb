class Monster < ActiveRecord::Base

  SUB_CLASSES = %w(fire water earth electric wind)

  belongs_to :user
  belongs_to :monster_team

  before_save :define_weakness

  validates :name, :power, :sub_class, :user_id, presence: true
  validates :sub_class, inclusion: { in: SUB_CLASSES }
  validate :validate_monsters_number, on: :create

  private

  def validate_monsters_number
    if user && user.monsters.count >= 20
      errors.add(:base, 'Monsters\' number is exceeded. ' \
                        'User should not have more than 20 monsters')
    end
  end

  def define_weakness
    sub_class_index = SUB_CLASSES.index(sub_class)
    self.weakness = find_weakness(sub_class_index)
  end

  def find_weakness(sub_class_index)
    if sub_class_index == (SUB_CLASSES.length - 1)
      SUB_CLASSES.first
    else
      SUB_CLASSES[sub_class_index + 1]
    end
  end
end
