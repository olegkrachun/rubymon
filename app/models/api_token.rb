require 'securerandom'

class ApiToken < ActiveRecord::Base

  before_create :set_token

  validates :name, presence: true

  private

  def set_token
    begin
      self.access_token = SecureRandom.uuid
    end while self.class.where(access_token: access_token).any?
  end
end
